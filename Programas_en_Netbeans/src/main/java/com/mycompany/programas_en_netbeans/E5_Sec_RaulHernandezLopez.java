/*
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 21 de Noviembre del 2020
 * Grupo 3012
 */
package com.mycompany.programas_en_netbeans;
import javax.swing.JOptionPane;
/**
 *Calcular el nuevo salario de un empleado si obtuvo un incremento del 8% 
 *sobre su salario actual y un descuento de 2.5% por servicios.
 */
public class E5_Sec_RaulHernandezLopez {
    public static void main(String args[]){
        //Declaracion de variables
        double sueldo_base;
        double incremento = 0.08;
        double descuento = 0.025;
        double incremento_del_ocho_porciento;
        double sueldo_con_descuento;
        double sueldo_incremento;
        double sueldo_final;
        
        //Entrada de datos.
        sueldo_base = Float.parseFloat(JOptionPane.showInputDialog
        ("Ingresa tu sueldo base"));
        incremento_del_ocho_porciento = sueldo_base * incremento;
        sueldo_incremento = sueldo_base + incremento_del_ocho_porciento;
        sueldo_con_descuento = sueldo_incremento * descuento ;
        sueldo_final = sueldo_incremento - sueldo_con_descuento;
        
        JOptionPane.showMessageDialog
        (null , "Se te otorgo un incremeto!" 
                + "\nsueldo anterior $" + sueldo_base 
                + "\nIncremento del 8% $" + incremento_del_ocho_porciento
                + "\nDescuento del 2.5% por servicios $" + sueldo_con_descuento
                + "\nMonto a cobrar $" + sueldo_final);
    }
}
