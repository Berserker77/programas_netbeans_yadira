/* 21 de Noviembre del 2020
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * Grupo 3012
 */
package com.mycompany.programas_en_netbeans;

import javax.swing.JOptionPane;

/*
 *Calcula el precio de un boleto de viaje, tomando en cuenta el número 
 *de kilómetros que se van a recorrer. El precio por Kilometro es de
 *$20.50.
 */
public class E2_CondSimp_RaulHernandezLopez {
    public static void main (String args []){
        //Declaracion de variables
        double kilometros;
        double costo = 20.5;
        double precio_boleto;
        int seleccion;
        String destino;
        
        //Entrada de datos
        JOptionPane.showMessageDialog
        (null, "ESTACION DE AUTOBUCES 'SION'\nBienvenido!\n" 
        + "El costo por kilometro de viaje es de $20.50");
   
        destino = JOptionPane.showInputDialog
        ("Nombre de tu destino :");
        
        kilometros = Float.parseFloat(JOptionPane.showInputDialog
        ("Distancia en kilometros de tu destino"));
        //Determina el costo del boleto.
        precio_boleto = kilometros * costo;
        //Despliega un mensaje con el costo del boleto.   
        JOptionPane.showMessageDialog
        (null, "El costo de tu boleto a " + destino + "\nEs de $" + precio_boleto );
        //Despliega un menu en donde 2 = No y 1 = Yes.
        seleccion = Integer.parseInt(JOptionPane.showInputDialog
        ("Deseas proceder con la compra? \n1.-yes\n2.-No"));
        
        if (seleccion == 1){
            JOptionPane.showMessageDialog(null, "Disfruta tu viaje!");
        }
        else if (seleccion == 2){
            JOptionPane.showMessageDialog(null, "Hasta pronto.");
        }
    }
}    

