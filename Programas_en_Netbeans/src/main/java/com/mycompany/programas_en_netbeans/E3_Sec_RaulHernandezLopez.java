/*
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 20 de noviembre del 2020
 * Grupo 3012
*/
package com.mycompany.programas_en_netbeans;

import javax.swing.JOptionPane;

/*
 *Calcular el cambio de monedas en dólares y euros al ingresar cierta cantidad 
 *en pesos. (Tipo de cambio Euros:22.50 ). 
 */
public class E3_Sec_RaulHernandezLopez {
     public static void main (String args[]){            
        //Declaracion de variables.
        double cambio_dolares;
        double cambio_euros;
        double pesos;
        double dolares = 22.09;
        double euros = 22.50;
        //Entrada de datos.
        pesos = Float.parseFloat(JOptionPane.showInputDialog
        (null, "Ingresa el numero de pesos que tienes $"));        
        
        cambio_dolares = pesos/dolares;
        JOptionPane.showMessageDialog
        (null, "La cantidad de pesos que tienes es igual a :\n" + cambio_dolares + "Dolares");  
             
        cambio_euros = pesos/euros;
        JOptionPane.showMessageDialog
        (null, "La cantidad de pesos que tienes es igual a :\n" + cambio_euros + "Euros");                   
    }//Fin metodo principal
    
}
