/*
 *HERNANDEZ_LOPEZ_RAUL_@Neo
 *freeenergy1975@gmail.com
 *19 de Noviembre del 2020
 *Grupo 3012
 */
package com.mycompany.programas_en_netbeans;
import javax.swing.JOptionPane;
/**
 *Calcula el precio de un boleto de viaje, tomando en cuenta el número 
 *de kilómetros que se van a recorrer. El precio por Kilometro es de
 *$20.50.
 */
public class E2_Sec_RaulHernandezLopez {
    public static void main (String args []){
        //Declaracion de variables
        double kilometros;
        double costo = 20.5;
        double precio_boleto;
        String destino;
        
        //Entrada de datos
        JOptionPane.showMessageDialog
        (null, "ESTACION DE AUTOBUCES 'SION'\nBienvenido!\n" 
        + "El costo por kilometro de viaje es de $20.50");
        
        destino = JOptionPane.showInputDialog("Nombre de tu destino :");
        
        kilometros = Float.parseFloat(JOptionPane.showInputDialog
        ("Distancia en kilometros de tu destino"));
        
        precio_boleto = kilometros * costo;
        
        JOptionPane.showMessageDialog
        (null, "El costo de tu boleto a " + destino + "\nEs de $" + precio_boleto
        + "\nBuen viaje!");
    }
}