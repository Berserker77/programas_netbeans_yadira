/*
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 20 de noviembre del 2020
 * Grupo 3012
 */
package com.mycompany.programas_en_netbeans;
import javax.swing.JOptionPane;
/*
 *Una persona recibe un préstamo de $10,000.00 de un banco y desea saber cuánto 
 * pagará de interés, si el banco le cobra una tasa del 27% anual.
 */
public class E03_CondSimp_Raul_HernandezLopez {
    public static void main(String args[]){
        //Declaracion de variables
        double dolares = 22.09;
        double euros = 21.91;
        double conversion_dolares;
        double conversion_euros;
        double pesos; 
        int seleccion;
        int continuar;
       
        do {
            //Entrada de datos.
            pesos = Float.parseFloat(JOptionPane.showInputDialog
            ("Ingresa el numero de pesos que tienes"));
            
            //Despliega un menu.
            seleccion = Integer.parseInt(JOptionPane.showInputDialog
            ("Que deceas hacer ?:\n"
            + "1.- Calcular en Euros\n"
            + "2.- Calcular en Dolares"));
            
            //Realiza la conversion de Pesos a Dolares.
            if (seleccion == 1){
                //Define el numero de pesos de acuerdo al  numero de pesos.
                conversion_dolares = pesos/dolares;
                //Saliada de datos.
                JOptionPane.showMessageDialog
                (null, pesos + " es igual a :" + conversion_dolares 
                + " dolares.");
            }
            //Realiza la conversion de Pesos a Euros.
            else if(seleccion == 2){
                //Define el numero de euros de acuerdo al numero de pesos.
                conversion_euros = pesos/euros;
                //Salidad de datos.
                JOptionPane.showMessageDialog
                (null, pesos + " es igual a :" + conversion_euros 
                + " Euros.");
            }
            //Le da la opcion al usuario de Ejecutar de nuevo la aplicacion.
            continuar = Integer.parseInt(JOptionPane.showInputDialog
            ("Deseas continuar?\n 1.-yes\n2.-No"));
        }while(continuar == 1);
    }
}
