/*
 * RAUL_HERNANDEZ_LOPEZ @Neo
 * freeenergy1975@gmail.com
 * 23 de Noviembre del 2020
 * Grupo 3012
 */
package com.mycompany.programas_en_netbeans;
import javax.swing.JOptionPane;
/*
 *Calcula el precio de un boleto de viaje, tomando en cuenta el número de 
 *kilómetros que se van a recorrer. El precio por Kilometro es de $20.50.
 */
public class E2_CondMult_RaulHernandezLopez {
    public static void main (String args[]){
        
        //Declaracion de variables
        double precio = 20.5;
        double distancia;
        double monto_a_pagar;
        String destino;
        String decision;
        
        //Entrada de datos
        JOptionPane.showMessageDialog(null, "Estacion de autobuces"
                + "\nBienvenido"
                + "\nNuestra tarifa por kilometros es de $20.5");
        
        destino = JOptionPane.showInputDialog("A donde quieres viajar :");
        
        distancia = Float.parseFloat(JOptionPane.showInputDialog
        ("Distancia en kilometros de tu destino :"));
        
        //Determina el costo del boleto
        monto_a_pagar = precio * distancia;
        
        JOptionPane.showMessageDialog(null, "El costo de tu boleto es de $"
        + monto_a_pagar);
        //Muestra un pequeño menú al usuario
        decision = JOptionPane.showInputDialog
        ("Deseas proceder con la compra del boleto?" + "yes/no");
        
        //Despliegue de mensajes segun la decision del usuario
        if(decision.equalsIgnoreCase("yes")){
            JOptionPane.showMessageDialog(null, "Que tengas buen viaje a "
            + destino);
        }
        else if (decision.equalsIgnoreCase("no")){
            JOptionPane.showMessageDialog(null, "Que tengas un buen dia!");
        }
        else{
            JOptionPane.showMessageDialog
        (null, "Repite el proceso e ingresa una opcion valida");
        }
    }//fin metodo main
}//fin clase E1_CondMult_RaulHernandezLopez
