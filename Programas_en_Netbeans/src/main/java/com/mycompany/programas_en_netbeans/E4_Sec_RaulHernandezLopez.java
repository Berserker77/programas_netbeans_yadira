/*
 *HERANDEZ_LOPEZ_RAUL @Neo
 *freeenergy1975@gmail.com
 *21 de Noviembre del 2020
 *Grupo 3012
 */

package com.mycompany.programas_en_netbeans;
import javax.swing.JOptionPane;
/*
 *Calcular el descuento y el monto a pagar por un medicamento cualquiera en 
 *una farmacia si todos los medicamentos tienen un descuento del 35%.
 */
public class E4_Sec_RaulHernandezLopez {
    public static void main (String args[]){
        //Declaracion de variables.
        String medicamento;
        double descuento = 0.35;
        double precio_real;
        double monto_final;
        double medicamento_descuento;
        //Entrada de datos.
        medicamento = JOptionPane.showInputDialog
        ("Ingresa el nombre del medicamento");
        
        precio_real = Float.parseFloat
        (JOptionPane.showInputDialog("Precio del producto $"));
        //Determina el valor del descuento.
        medicamento_descuento = precio_real * descuento;
        //Determina el monto a pagar por el medicamento.
        monto_final = precio_real - medicamento_descuento;
        //Impresion de resultados.
        JOptionPane.showMessageDialog(null, medicamento  
                + "\nPrecio real $" + precio_real 
                + "\nDescuento $" + medicamento_descuento 
                + "\nMonto a pagar $" + monto_final);
    }
}
