/**
 *HERNANDEZ_LOPEZ_RAUL. @Neo.
 *freeenergy1975@gmail.com.
 * 19 de noviembre del 2020.
 * Grupo_3012
 */
package com.mycompany.programas_en_netbeans;

import javax.swing.JOptionPane;
/*
 *Una persona recibe un préstamo de $10,000.00 de un banco y 
 *desea saber cuánto pagará de interés, si el banco le cobra 
 *una tasa del 27% anual.
 */
public class E1_Sec_RaulHernandezLopez {
    public static void main (String args[]){
        
        //Declaracion de variables.
        int fecha_prestamo;
        int fecha_limite;
        int tiempo_total;
        int x;
        double prestamo;
        double tasa_interes = 0.27;
        
        //Entrada de datos.
        fecha_prestamo = Integer.parseInt(JOptionPane.showInputDialog
        ("Fecha de Prestamo (ejemplo 2001) :"));
        
        fecha_limite = Integer.parseInt(JOptionPane.showInputDialog
        ("Cual es la fecha limite? (ejemplo 2002) :"));
        
        prestamo = Float.parseFloat(JOptionPane.showInputDialog
        ("Cual fue el monto de tu prestamo? $"));
        
        tiempo_total = fecha_limite - fecha_prestamo;
        
        for(x = 1; x <= tiempo_total; x++){
            //determina la fecha.
            fecha_prestamo += 1;
            
            //Determina el monto a pagar con un interes del 27% anual.
            prestamo = prestamo + (prestamo * tasa_interes);           
            
            //Imprime resultados.
            JOptionPane.showMessageDialog(null, "fecha [" + fecha_prestamo 
            +"] Monto a pagar $" + prestamo);            
            
        }//Fin ciclo for
    }//Fin metodo main    
}//Fin clase E1_Sec_Raul_Hernandez_Lopez
