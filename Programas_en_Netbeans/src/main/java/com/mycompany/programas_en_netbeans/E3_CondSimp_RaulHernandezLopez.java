/*
*HERNANDEZ_LOPEZ_RAUL @Neo
*freeenergy1975@gmail.com
*19 de Noviembre del 2020
*Grupo 3012
*/
package com.mycompany.programas_en_netbeans;

import javax.swing.JOptionPane;

/*
*Calcular el cambio de monedas en dólares y euros al ingresar
*cierta cantidad en pesos. (Tipo de cambio Euros:22.50 ).
 */
public class E3_CondSimp_RaulHernandezLopez {    
    public static void main (String args[]){            
        //Declaracion de variables.
        double cambio;
        double pesos;
        double dolares = 22.09;
        double euros = 22.50;
        int seleccion;
        //Entrada de datos.
        pesos = Float.parseFloat(JOptionPane.showInputDialog
        (null, "Ingresa el numero de pesos que tienes $"));
        
        //Despliega un menu.
        seleccion = Integer.parseInt(JOptionPane.showInputDialog
        ("Que quieres hacer\n1.- Convertir a dolares\n2.- Convertir a Euros"));
        
        if (seleccion == 1){
            cambio = pesos/dolares;
            JOptionPane.showMessageDialog
            (null, "La cantidad de pesos que tienes es igual a :" + cambio + "Dolares");  
        }
        else if(seleccion == 2){
            cambio = pesos/euros;
            JOptionPane.showMessageDialog
            (null, "La cantidad de pesos que tienes es igual a :" + cambio + "Euros");   
        }
        
    }//Fin metodo principal
    
   
}