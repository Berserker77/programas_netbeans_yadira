/*
 * RAUL_HERNANDEZ_LOPEZ
 * freenergy1975@gmail.com
 * 23 de Noviembre del 2020
 * Grupo 3012
 */
package com.mycompany.programas_en_netbeans;

import javax.swing.JOptionPane;

/*
 * Una persona recibe un préstamo de $10,000.00 de un banco y desea saber 
 * cuánto pagará de interés, si el banco le cobra una tasa del 27% anual. 
 */
public class E1_CondMult_RaulHernandezLopez {

    public static void main(String args[]) {
        
        //Declaracion de variables.
        double tasa_interes = 0.27;
        double prestamo;
        int fecha_inicial,
            fecha_final,
            tiempo_transcurrido,
            x;
        String decision,
               positivo = "yes",
               negativo = "no";
        
        //Entrada de datos
        prestamo = Float.parseFloat(JOptionPane.showInputDialog
        ("Ingresa el monto que deseas que se te preste"));
        
        fecha_inicial = Integer.parseInt(JOptionPane.showInputDialog
        ("Ingresa la fecha, ejemplo (2020) :"));
        
        fecha_final = Integer.parseInt(JOptionPane.showInputDialog
        ("Ingresa la fecha limite en que tendras para liquidar el prestamo :"));
        //Determina el número de años en los que se aplica el interes.
        tiempo_transcurrido = fecha_final - fecha_inicial;
        //Despliega un pequeño menú de opiciones
        decision = JOptionPane.showInputDialog
                (null, "Nuestra tasa de interes anual es del 27%"
                + "\nDeseas continuar ?"
                + "\nyes/no.");
        
        if (decision.equalsIgnoreCase(positivo)) {
            //Determina el monto a pagar cada fecha.
            for (x = 0; x < tiempo_transcurrido; x++){
                fecha_inicial = fecha_inicial + 1;
                prestamo = prestamo + (prestamo * tasa_interes);
                //Imprime resultados.
                JOptionPane.showMessageDialog
                (null, fecha_inicial + " Monto a pagar $" + prestamo);
                
            }
        } else if (decision.equalsIgnoreCase(negativo)) {
            JOptionPane.showMessageDialog
            (null, "De acuerdo Buen dia!");
        } else {
            JOptionPane.showMessageDialog
            (null, "Ingresa una opcion valida por favor");
        }
    }//Fin metodo main
}//Fin de la clase E1_CondMult_RaulHernandezLopez
