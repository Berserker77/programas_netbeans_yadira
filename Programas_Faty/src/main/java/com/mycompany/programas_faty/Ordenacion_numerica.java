/*
 * HERNANDE_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 26 de noviembre del 2020
 * Tema: Estructuras condicionales
 * Crea un programa que dado tres numeros enteros determine cual es el mayor
 * y menor de ellos.
 */
package com.mycompany.programas_faty;

import javax.swing.JOptionPane;

public class Ordenacion_numerica {
    public static void main (String Neo[]){
        int x,
            y,
            cambio_posicion;
            
        int numeros[] = new int[3];
            
        //Entrada de datos
        numeros[0] = Integer.parseInt(JOptionPane.showInputDialog
        ("Ingresa un numero entero"));
        
        numeros[1] = Integer.parseInt(JOptionPane.showInputDialog
        ("Ingresa un segundo numero"));
        
        numeros[2] = Integer.parseInt(JOptionPane.showInputDialog
        ("Ingresa un numero mas"));
            
        for(x = 0; x < 3; x++){
            for (y = 0; y < 2; y++){
                if(numeros[y] > numeros[y+1]){
                    cambio_posicion = numeros[y + 1];
                    numeros[y + 1] = numeros[y];
                    numeros[y] = cambio_posicion;
                }//fin condicional                  
            }//Fin ciclo for 2
        }//fin ciclo for 1
        
       
        JOptionPane.showMessageDialog(null, "El numero " + numeros[0] 
                    + " es el menor de todos");
        
        JOptionPane.showMessageDialog(null, "El numero " + numeros[1] 
                + "  no es ni el mayor ni menor");
     
        JOptionPane.showMessageDialog(null, "El numero " + numeros[2] 
                   + " es el mayor de todos");   
    }//Fin del metodo principal
}//Fin de la clase Ordenacion_numerica
