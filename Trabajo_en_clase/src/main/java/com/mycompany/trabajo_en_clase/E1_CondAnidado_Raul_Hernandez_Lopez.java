/*
 * HERNANDE_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 02/12/2020
 * TEMA: Condicionales anidadas 
 * Programa que determina si una persona es apta para votar 
 * si es mayor de edad, cuenta con credencial para votar, es vigente
 * y si es que se encuentra en el patron electoral de no cumplir con 
 * las especificaciones se considera como no apto.
 */
package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;

public class E1_CondAnidado_Raul_Hernandez_Lopez {
    public static void main(String Neo[]){
        //Declaracion de variables
        int edad,
            vigencia;
        String credencial,
            patron_electoral;
        
        edad = Integer.parseInt(JOptionPane.showInputDialog("Ingresa tu edad"));
        
        if (edad >= 18){
            credencial = JOptionPane.showInputDialog("Cuentas con credencial "
                    + "para votar Yes/No");
            if (credencial.equalsIgnoreCase("yes")){
                vigencia = Integer.parseInt(JOptionPane.showInputDialog
                ("Ingresa la fecha de vigencia ejemplo (2001)"));
                if (vigencia >= 2006){
                    patron_electoral = JOptionPane.showInputDialog
                    ("Se encuentra el patron electoral yes/no");
                    if (patron_electoral.equalsIgnoreCase("yes")){
                        JOptionPane.showMessageDialog
                        (null, "Genial, puedes votar.");
                    }//Fin if 4
                    else {
                        JOptionPane.showMessageDialog(null, "No eres apto para votar");
                    }//Fin else 4
                }//Fin if 3
                else{
                    JOptionPane.showMessageDialog(null, "No eres apto para votar");
                }//Fin else 3
            }//Fin if 2
            else{
                 JOptionPane.showMessageDialog(null, "No eres apto para votar" 
                    + " Tramita tu credencial para votar.");
            }//Fin else 2
        }//Fin if 1
        else{
            JOptionPane.showMessageDialog(null, "No eres apto para votar" 
                    + " aun eres menor de edad.");
        }//Fin else 1
    }//Fin del metodo principal    
}//Fin clase E1_CondAnidado_RaulHernandeLopez
