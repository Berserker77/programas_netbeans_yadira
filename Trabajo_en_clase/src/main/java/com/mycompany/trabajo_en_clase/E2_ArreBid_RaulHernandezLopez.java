/*
 * HERNANDEZ_LOPEZ_RAUL @Neo.
 * 20 de Diciembre del 2020.
 * freeenergy1975@gmail.com
 * Grupo: 3012   Carrera: Sistemas computacionales.
 * Tema: Arreglos Bidimencionales, solicitar la calificacion de las materias
 * , las suma y saca el promedio.
 */
package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;


public class E2_ArreBid_RaulHernandezLopez {
        public static void main ( String Neo[]){
        //Declaracion de variables.
        byte numeroMaterias,
             numeroUnidades = 5,
             numero_materia = 0;
        int x, y, longitud;
        float promedioUnidades,
              sumaPromedioMaterias = 0,
              promedioGeneral;
        String nombre,
               grupo,
               carrera,
               unid[] = {"U1", "U2", "U3", "U4", "U5"};
                
        //Entrada de datos
        nombre = JOptionPane.showInputDialog("Nombre");
        
        grupo = JOptionPane.showInputDialog("Grupo");
        
        carrera = JOptionPane.showInputDialog("Carrera");
        
        //Obtiene el numero de alumnos.
        numeroMaterias = Byte.parseByte(JOptionPane.showInputDialog("Numero "
                + "de materias"));        
        
        //Asigna los espacios de memoria que se utilizararan para almacenas calificaciones. 
        float calificacionesUnidades[][] = new float [numeroMaterias][numeroUnidades];
        
        //Asiga los espacios de memoria para los nombres de las materias.
        String nombresMaterias[] = new String [numeroMaterias];
        
        System.out.print("Nombre del alumno: " + nombre
                + "\nCarrera: " + carrera + "\nGrupo: " + grupo + "\n"
                + "\t\t" + unid[0] + "\t" + unid[1] + "\t" + unid[2] + "\t" + unid[3] 
                + "\t" + unid[4]+ "\tPromedio");
        
        for(x = 0; x < numeroMaterias; x++){
            numero_materia += 1;
            
            //Delimita el numero de caracteres para que no altere la impresion de resultados
            do{
            //Almacena el nombre las materias 
            nombresMaterias[x] = JOptionPane.showInputDialog("Nombre de la materia No."
                     + numero_materia + "\n(El numero de caracteres no debe ser mayor a 16)");
            longitud = nombresMaterias[x].length();
            }while(longitud > 16);
            
            //Declaracion de variables
            int posicion = 0;
            float sumaCalificaciones = 0;
            
            System.out.print("\n" + nombresMaterias[x]);
            
            for(y = 0; y < numeroUnidades; y++){
                
                //muestra la inidad en la que se encuentra
                posicion += 1;
            
                //almacena el valor de la calificacion por unidad
                calificacionesUnidades[x][y] = Float.parseFloat(JOptionPane.showInputDialog
                ("Ingresa la calificacion de la unidad [" + posicion + "]"));
            
                System.out.print("\t" + calificacionesUnidades[x][y]);
            
                //almacena el valor de la suma de las calificaciones
                sumaCalificaciones += calificacionesUnidades[x][y];
            
            }//Fin ciclo for 2, manipula el numero de columnas.
            
            //Determina el promedio de las de totas la unidades de una materia
            promedioUnidades = sumaCalificaciones / numeroUnidades;
            
            //Suma el promedio de las materias
            sumaPromedioMaterias += promedioUnidades;
            
            //Impresion de resultados.
            System.out.print("\t[" + promedioUnidades + "]");
            
        }//Fin ciclo for encargado de manipular las filas.
        
        //Determina el valor del promedio general
        promedioGeneral = sumaPromedioMaterias / numeroMaterias;
        
        //Impresion de resultados
        System.out.print("\n\n\t\t\t\t\tPromedio General ["+ promedioGeneral + "]");
        
    }//Fin del metodo principal
}//Fin de la clase E2_ArregBid_RaulHernandezLopez