/*
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 24 de noviembre del 2020
 * Grupo 3012
 * TEMA: Estructuras de condicion doble
 * EN EL TECNOLOGICO NACIONAL DE MEXICO SE PUBLICO UNA
   CONVOCATORIA DE ESTUDIOS EN EL EXTRANJERO, PODRAN PARTICIPAR AQUELLOS
   ESTUDIANTES QUE TENGAN UN PROMEDIO MAYOR A 90. LOS ALUMNOS QUE HAYAN
   ALCANZADO LA CALIFICACION ENTRARAN DIRECTO A LA TOMBOLA PARA EL SORTEO.
   LOS ALUMNOS QUE NO ALCANCEN EL PROMEDIO PODRAN PARTICIPAR EN LA SIGUIENTE
   CONVOCATORIA SI APRUEBAN EL EXAMEN DEL IDIOMA PARA EL SIGUIENTE SEMESTRE
   Y MEJORAN EN UN 20% SU PROMEDIO.
 */
package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;

public class E2_CondDobleAlt_RaulHernandezLopez {
    public static void main (String args[]){
        
        double promedio_examen,
               calificaciones [];
           int numero_alumnos,
               x,
               y;
        String nuevos_candidatos, 
               nombres[];
        
        //Determina el numero de alumnos.
        numero_alumnos = Integer.parseInt(JOptionPane.showInputDialog
        ("Ingrese el numero de alumnos que tienes"));
        
        /*Determina la cantidad de nombres y calificaciones que almacenara 
        el array*/
        calificaciones = new double[numero_alumnos];
        nombres = new String[numero_alumnos];
        
        for(x = 0; x < numero_alumnos; x++){//Inicio ciclo for 1
            //Guarda el nombre del alumno en el array.
            nombres[x] = JOptionPane.showInputDialog("Nombre del alumno :");
            //Guarda la calificacion del alumno en el array calificaciones.
            calificaciones[x] = Float.parseFloat(JOptionPane.showInputDialog
            ("Calificacion del alumno(1/100)"));
            
            //Determina que alumno aplica para la convocatoria y cual no.
            if((calificaciones[x] >= 90) && (calificaciones[x] <= 100)){
                
                JOptionPane.showMessageDialog
                (null, "El alumno " + nombres[x] 
                + " Aplica para la convocatoria");
            }
            
            else{
                JOptionPane.showMessageDialog
                (null, "El alumno " + nombres[x] 
                + " No aplica para la convocatoria");
            }
        }//Fin ciclo for 1
          /*Recorre todo el array en busca de nuevos 
          candidatos para la nueva convocatoria*/
          for(y = 0; y < numero_alumnos; y++){//Inicio ciclo for 2
             /*Selecciona a los alumnos que no aplicaron para la convocatoria
              pasada*/
             if(calificaciones[y] < 90){
                 //seleccion 
                 nuevos_candidatos = JOptionPane.showInputDialog
                 ("El alumno " + nombres[y] +" aprobo el examen de idiomas "
                 + "y mejoro un 20 % "
                 + "su calificacion ? yes/no");
                 //Determina si el nuevo candidato aplica o no 
                 if(nuevos_candidatos.equalsIgnoreCase("yes")){
                     JOptionPane.showMessageDialog
                     (null, "El alumno " + nombres[y] 
                     + " aplica para la convocatoria");                 
                 }
                 else{
                     JOptionPane.showMessageDialog
                     (null, "El alumno " + nombres[y] + " no aplica");
                 }
             }
        }//Fin ciclo for 2
    }//Fin del metodo main
}//Fin de la clase E2_CondDobleAlt_RaulHernandezLopez
