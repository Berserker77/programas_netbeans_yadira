/*
 * RAUL_HERNANDEZ_LOPEZ @Neo
 * freeenrgy1975@gmail.com
 * 24 de noviembre del 2020
 * Gropo 3012
 * TEMA: estructura condicional multiple
 * Genera un programa que determine si la calificacion es aprobatoria o no 
 */
package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;
        
public class E1_CondDobleAlt_HernandezLopezRaul {
    public static void main(String args[]){
        //Declaracion de variables
        double calificacion;
        String materia;
        
        //entrada de datos
        materia = JOptionPane.showInputDialog("Nombre de la materia");
                
        calificacion = Float.parseFloat(JOptionPane.showInputDialog
        ("Ingresa tu calificacion 0/100"));
        
        if (calificacion >= 70){
            //muestra un mensaje de aprobacion
            JOptionPane.showMessageDialog
            (null, "Felicidades tu calificacion en " + materia 
                    + "es aprobatoria");
        }   
        else if (calificacion < 70){
            //muestra un mensaje en caso de que la calificacion no sea aprobatoria
            JOptionPane.showMessageDialog
            (null, "Lo lamento tu calificacion en" + materia 
                    +  " no es aprobatoria ");
        }
    }//Fin metodo main
}//Fin de la clase E1_CondDobleAlt_RaulHernandezLopez
