/*
 * HERNANDEZ_LOPEZ_RAUL @Neo.
 * 17 de Diciembre del 2020.
 * freeenergy1975@gmail.com
 * Grupo: 3012   Carrera: Sistemas computacionales.
 * Tema: Arreglos Bidimencionales, solicitar la calificacion de las materias
 * , las suma y saca el promedio.
 */
package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;

public class E1_ArregBid_RaulHernandezLopez {
    public static void main ( String Neo[]){
        //Declaracion de variables.
        byte numeroMaterias,
             numeroUnidades = 5;
        int x, y;
        float promedioUnidades,
              sumaPromedioMaterias = 0,
              promedioGeneral;
            
        //Obtiene el numero de alumnos.
        numeroMaterias = Byte.parseByte(JOptionPane.showInputDialog("Numero "
                + "de materias"));
        
        System.out.print("\t\tUNIDADES\n");
        
        //Asigna los espacios de memoria que se utilizararan para almacenas calificaciones. 
        float calificacionesUnidades[][] = new float [numeroMaterias][numeroUnidades];
        
        //Asiga los espacios de memoria para los nombres de las materias.
        String nombresMaterias[] = new String [numeroMaterias];
        
        for(x = 0; x < numeroMaterias; x++){
            
            //Almacena el nombre las materias
            nombresMaterias[x] = JOptionPane.showInputDialog("Nombre de la materia");
            
            //Declaracion de variables
            int posicion = 0;
            float sumaCalificaciones = 0;
            
            System.out.print("\n\n\n" + "Materia: " + nombresMaterias[x] 
                    + "\n\nUni\tUni\tUni\tUni\tUni"
                    + "\n_____________________________________"
                    + "\n\n  1\t  2\t  3\t  4\t  5\n"
                    + "\n_____________________________________\n");
            
            for(y = 0; y < numeroUnidades; y++){
                
                //muestra la inidad en la que se encuentra
                posicion += 1;
            
                //almacena el valor de la calificacion por unidad
                calificacionesUnidades[x][y] = Float.parseFloat(JOptionPane.showInputDialog
                ("Ingresa la calificacion de la unidad [" + posicion + "]"));
            
                System.out.print(calificacionesUnidades[x][y] + "\t ");
            
                //almacena el valor de la suma de las calificaciones
                sumaCalificaciones += calificacionesUnidades[x][y];
            
            }//Fin ciclo for 2, manipula el numero de columnas.
            
            //Determina el promedio de las de totas la unidades de una materia
            promedioUnidades = sumaCalificaciones / numeroUnidades;
            
            //Suma el promedio de las materias
            sumaPromedioMaterias += promedioUnidades;
            
            //Impresion de resultados.
            System.out.print("\n\n          " + "Promedio  " + "[" + promedioUnidades + "]\n");
            
        }//Fin ciclo for encargado de manipular las filas.
        
        //Determina el valor del promedio general
        promedioGeneral = sumaPromedioMaterias / numeroMaterias;
        
        //Impresion de resultados
        System.out.print("\n\n\tPromedio General ["+ promedioGeneral + "]");
        
    }//Fin del metodo principal

}//Fin de la clase E1_ArregBid_RaulHernandezLopez
