/*
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 8 de diciembre del 2020
 * Grupo 3012
 * DESCRIPCION : CALCULAR EL PROMEDIO DE LAS EDADES DEL GRUPO DE PRIMER SEMESTRE
 */
package com.mycompany.trabajo_en_clase;


import javax.swing.JOptionPane;

public class E1_EstIterativasFor_Raul_Hernandez_Lopez {    
    public static void main(String Neo[]){
        //Declaracion de variables
        int posicion = 0,
            numeroAlumno,
            sumEdades = 0,
            x;
        byte edad;
        double promedio;
       //Determina el numero de alumnos
       numeroAlumno = Integer.parseInt(JOptionPane.showInputDialog
        ("Numero de alumnos"));
       
       //recolecta el valor de las edades y la suma.
       for(x =0; x < numeroAlumno; x++){
           posicion = posicion + 1;
           //Obtiene el valor de las edades
           edad = Byte.parseByte(JOptionPane.showInputDialog
           ("Ingresa la edad del alumno [" + posicion + "]"));
           //Determina la suma de las edades.
           sumEdades = sumEdades + edad;
       }//Fin for
       
       //Determina el promedio de las edades
       promedio =  sumEdades/numeroAlumno;
       
       //Impresion de resultados
       JOptionPane.showMessageDialog(null, "el promedio del grupo es :" + promedio);
       
    }//Fin metodo principal
}//Fin clase E1_EstIterativasFor_Raul_Hernandez_Lopez
