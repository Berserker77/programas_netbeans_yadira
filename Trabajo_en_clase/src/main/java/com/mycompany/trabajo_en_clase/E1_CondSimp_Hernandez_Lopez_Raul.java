/*
 * HERNANDE_LOPEZ_RAUL
 * freeenergy1975@gmail.com
 * Grup0: 3012
 *TEMA: Estructura consicional simple
 *Genera un programa que determine si eres de mayor de edad... En este caso 
 *implemente una estructura condicional multiple pero la comente para que 
 *solo se ejecute la condicional simple.
 */
package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;

public class E1_CondSimp_Hernandez_Lopez_Raul {
   public static void main(String args[]){
        //Declaracion de variables.
        int fecha_nacimiento,
            fecha_actual,
            edad;
        //Entrada de datos
        fecha_nacimiento = Integer.parseInt(JOptionPane.showInputDialog("Ingresa tu fecha de nacimiento"));
        fecha_actual = Integer.parseInt(JOptionPane.showInputDialog("Ingresa la fecha actual"));
        edad = fecha_actual - fecha_nacimiento;
        
        if (edad >= 18){
            JOptionPane.showMessageDialog
            (null, "Eres mayor de edad");
        }
        /*else if  (edad < 18){
            JOptionPane.showMessageDialog
            (null, "Tienes " + edad + "por lo tanto no eres mayor de adad");
        }*/   
    }
}
