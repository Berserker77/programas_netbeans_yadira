/* 
 * HERNANDEZ_LOPEZ_RAUL
 * freeenergy1975@gmail.com
 * 16 de diciembre del 2020 
 * Grupo 3012
 * Tema: Arreglos unidimencionales
 * Realiza un priograma que capture N edades y muestre el promedio de la suma de
 * la misma
 */
package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;
/**
 *
 * @author Neo
 */
public class E1_ArreglosUni_RaulHernandezLopez {
    public static void main(String Neo[]){
        //Declarcion de variables
        byte numero_personas, posicion = 0;
        int x;
        short suma_estatura = 0;
        float promedio;
        JOptionPane.showMessageDialog
        (null, "A continuacion se capturaran estaturas");
        
        //Obtiene el valor que determinara el tamano de nuestro arreglo.
        numero_personas = Byte.parseByte(JOptionPane.showInputDialog
        ("Numero de personas a las que se les tomara la altura"));
        short estaturas[] = new short[numero_personas];
        
        //Captura de estaturas.
        for(x = 0; x < numero_personas; x++){
            
            //Determina la posicion en la que se encuentra.
            posicion += 1;
            
            //almacena los valores en un arreglo.
            estaturas[x] = Short.parseShort
            (JOptionPane.showInputDialog("[" + posicion + "] Estatura en cm"));
            
            System.out.print("Estatura No " + posicion + "[" + estaturas[x] + "]\n");
            
            //acomula los valores de la estatura
            suma_estatura += estaturas[x];
        }//Fin ciclo for
        
        //Determan el promedio de las edades
        promedio = suma_estatura / numero_personas;
        
        //Impresion de resultados.
        JOptionPane.showMessageDialog(null, "El promedio de las edades es "
                + promedio);
        
    }//Fin del metodo principal
}//Fin de la clase E1_ArreglosUni_RaulHernandezLopez.
