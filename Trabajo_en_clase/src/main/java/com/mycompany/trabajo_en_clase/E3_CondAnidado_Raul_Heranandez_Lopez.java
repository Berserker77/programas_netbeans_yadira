/*
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 4/12/2020  Grupo: 3012
 * TEMA: Condicionales Anidadas.
 * Elabora un programa con condicionales anidados que solicite 3 calificaciones, 
 * obtén el promedio de esas tres calificaciones, y de acuerdo al promedio que 
 * se obtuvo, coloca la leyenda que le corresponde, que encontraras en la imagen 
 * que te comparto con el nombre NIVEL DE DESEMPENIO-TESJI.JPG.  
 * Por ejemplo si tu promedio se encuentra entre 95 y 100 deberá aparecer la 
 * leyenda "EXCELENTE" 
 */

//Si Puedes Imaginarlo Puedes Programarlo

package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;

public class E3_CondAnidado_Raul_Heranandez_Lopez {
    public static void main(String args[]){
        //Declaracion de variables
        double calificacion1,
               calificacion2, 
               calificacion3,
               promedio;
        String respuesta,
               respuestaExelencia,
               consejos;
        
        JOptionPane.showMessageDialog
        (null, "Competencias para un desempeño exelente\n"
                + "1.- Se adapata a sitaciones complejas\n"
                + "2.- Hace apotaciones a las actividades academicas\n"
                + "3.- Propone y/o explica suluciones no vistos en clase\n"
                + "4.- Promueve el pensamiento critico\n"
                + "5.- Incorpora conocimientos y actividades "
                     + "interdiciplinarias en su aprendizaje\n"
                + "6.- Realiza su trabajo de manera autonoma y autoregular");
        
        //Entrada de datos, es qui donde obtendremos los valres de las 3
        //calificaciones
        calificacion1 = Float.parseFloat(JOptionPane.showInputDialog
        ("Ingresa tu primera calificacion"));
        
        calificacion2 = Float.parseFloat(JOptionPane.showInputDialog
        ("Ingresa tu segunda calificacion"));
        
        calificacion3 = Float.parseFloat(JOptionPane.showInputDialog
        ("Ingresa tu ultima calificacion"));
     
        promedio = (calificacion1 + calificacion2 + calificacion3) / 3;
        
        //Determina si el alumno es exelente
        if ((promedio >= 95) && (promedio <= 100)){
            respuesta = JOptionPane.showInputDialog
            ( "Eres Exelente!\nQuires que ver las competencias que alcanzaste yes/no");
            
            if(respuesta.equalsIgnoreCase("yes")){
                respuestaExelencia = JOptionPane.showInputDialog
                ("Eres dios Bro, alcanzaste todas las competencias!"
                        + "\nQuieres dejar un par de consejos para los"
                        + " mortales yes/no");
                
                if (respuestaExelencia.equalsIgnoreCase("yes")){
                    consejos = JOptionPane.showInputDialog("vale escribe lo que quieras");
                    
                    JOptionPane.showMessageDialog(null, consejos + " tus palabras seran"
                            + " recordadas, Gracias");
                }
            }//fin if 2
            else {
                JOptionPane.showMessageDialog(null, "vale, bien echo campeon!");
            }//fin else 1
        }
        //Determina si el alumno es notable
        else if((promedio >= 85) && (promedio <= 94)){            
            respuesta = JOptionPane.showInputDialog("Eres Notable"
                    + "\nQuires que ver las competencias que alcanzaste yes/no");
            
            if(respuesta.equalsIgnoreCase("yes")){
                JOptionPane.showMessageDialog(null, "cumpliste con 4 "
                        + "de los indicativos en desempeño exelente");
            }//Fin if 2
            else{
                JOptionPane.showMessageDialog(null, "vale, bien echo campeon!"
                        + " vamos por mas!");
            }//Fin else 2
        }//fin else if.
        else if((promedio >= 75) && (promedio <= 84)){
            respuesta = JOptionPane.showInputDialog("Eres bueno!"
                    + "\nQuires que ver las competencias que alcanzaste yes/no");
            
            if (respuesta.equalsIgnoreCase("yes")){
                JOptionPane.showMessageDialog(null, "Cumpliste 3 de los "
                        + "indicadores definidos en desempeño exelente");
            }//Fin if 3
            else{
                JOptionPane.showMessageDialog(null, "vale, bien echo campeon!"
                        + " puedes hacerlo mejor, animo!");
            }//Fin else 3
        }//Fin else if 2
        else if ((promedio >= 70) && (promedio <= 74)){
            respuesta = JOptionPane.showInputDialog("Suficiente"
                    + "\nQuieres ver las conpetencias que alcanzate? yes/no");
            
            if(respuesta.equalsIgnoreCase("yes")){
                JOptionPane.showMessageDialog(null, "Cumpliste con dos de los "
                        + "indicadores definidos en desempeño exelente");
            }//Fin if 4
            else{
                JOptionPane.showMessageDialog(null, "vale tienes muchas areas"
                        + "de oportunidad !"
                        + "\nPuedes hacerlo mejor, animo!");
            }//Fin else 4
        }//Fin else if 3
        else if (promedio < 70 ){
                JOptionPane.showMessageDialog(null, "NA (no alcanza)"
                        + "\nLamento decir que tu desempeño dejo mucho que "
                        + "decear pero descuida no todo esta acabado puedes "
                        + "volver a levantarte, creo en ti!");
        }//Fin else if 4
    }//Fin del metodo principal.
}//Fin clase E1CondAnidado_Raul_Hernandez_Lopez.
