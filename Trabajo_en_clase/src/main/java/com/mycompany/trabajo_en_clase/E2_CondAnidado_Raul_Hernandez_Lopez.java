/*
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 5 de Diciembre del 2020
 * TEMA: Condicionales anidadas
 * Descripcion: Elabora un programa que compare tres numeros y los ordene de 
 * de mayor a menor
 */

//Si puedes imaginarlo puedes programarlo

package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;

public class E2_CondAnidado_Raul_Hernandez_Lopez {
    public static void main (String Neo[]){
        //Declaracion de varibles.
        int    numero1,
               numero2,
               numero3;
        //Entrada de dartos.
        numero1 = Integer.parseInt(JOptionPane.showInputDialog("Ingresa un "
                + "valor numerico entero"));
        
        numero2 = Integer.parseInt(JOptionPane.showInputDialog("Ingresa un "
                + "segundo valor numerico entero"));
        
        numero3 = Integer.parseInt(JOptionPane.showInputDialog("Ingresa el "
                + "ultimo valor numerico entero"));
        
        //Ordenas los numeros de mayor a menor segun se el caso.
        if(numero1 > numero2){
            if(numero1 > numero3){
                if(numero2 > numero3){
                    JOptionPane.showMessageDialog(null, numero1 
                            + ", " + numero2 + ", " + numero3);
                }//Fin if 3
                else{
                    JOptionPane.showMessageDialog(null, numero1 + ", " + numero3 
                        + ", " + numero2);
                }//Fin else 2
            }//Fin if 2
            else{
                JOptionPane.showMessageDialog(null, numero3 + ", " + numero1 
                        + ", " + numero2);
            }//Fin else 1
        }//Fin if 1
        else{
            if(numero2 > numero3){
                if(numero1 > numero3){
                    JOptionPane.showMessageDialog(null, numero2 + ", " 
                            + numero1 + ", " + numero3);
                }//Fin if 1.2
            }//Fin if 1.1
            else{
                JOptionPane.showMessageDialog(null, numero3 + ", " 
                        + numero2 + ", " + numero1);
            }//Fin else 1.2
        }//Fin else 1.1
    }//Fin metodo principal.
}//Fin clase E2_CondAnidinado_Raul_Hernandez_Lopez.
