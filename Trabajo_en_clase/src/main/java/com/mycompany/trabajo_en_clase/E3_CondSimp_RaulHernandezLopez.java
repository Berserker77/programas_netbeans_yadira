/*
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 24 de noviembre del 2020
 * Grupo 3012
 * TEMA: Programas con estructura condicional simple
 * UN PROGRAMA SOLICITA QUE SEAN CAPTURADOS TRES DATOS
 * NUMERICOS Y A PARTIR DE ELLOS IMPRIMIRA SI EL NUMERO ES PAR.
 */
package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;

public class E3_CondSimp_RaulHernandezLopez {
    public static void main(String args []){
        //Declaracion de variables
        int numero1,
            numero2,
            numero3;
        //Imprime un mensaje que le indica al usuario que ingrese valores numericos
        JOptionPane.showMessageDialog
        (null, "A continuacion se te pedirá que ingreses tres números "
                + "para determinar si son par o impar ");
        
        //pide el primer numero y determina si es par o impar
        numero1 = Integer.parseInt
        (JOptionPane.showInputDialog("Ingresa un numero"));
        //Divide el numero entre dos, si el residuo es 0 entonces es par
        if(numero1%2 == 0){
            JOptionPane.showMessageDialog(null, "el numero es par");
        }
        /*else{
            JOptionPane.showMessageDialog(null, "El numero es impar");
        }*/
        
        //pide el primer numero y determina si es par o impar
        numero2 = Integer.parseInt
        (JOptionPane.showInputDialog("Ingresa otro numero"));
        //Divide el numero entre dos, si el residuo es 0 entonces es par
        if(numero2%2 == 0){
            JOptionPane.showMessageDialog(null, "el numero es par");
        }
        /*else{
            JOptionPane.showMessageDialog(null, "El numero es impar");
        }*/
        
        //pide el primer numero y determina si es par o impar
        numero3 = Integer.parseInt
        (JOptionPane.showInputDialog("Ingresa otro numero"));
        //Divide el numero entre dos, si el residuo es 0 entonces es par
        if(numero3%2 == 0){
            JOptionPane.showMessageDialog(null, "el numero es par");
        }
        /*else{
            JOptionPane.showMessageDialog(null, "El numero es impar");
        }*/                       
    }
}
