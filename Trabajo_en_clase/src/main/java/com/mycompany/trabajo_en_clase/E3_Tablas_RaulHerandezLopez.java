/*
 * HERNANDEZ_LOPEZ_RAUL
 * freeenergy1975@gmail.com
 * 13 de Diciembre del 2020
 * Grupo: 3012
 * Tema: Imprime la tabla de multiplicar de un numero N.
 */
package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;

public class E3_Tablas_RaulHerandezLopez {
    public static void main(String Neo[]){
        //Declaracion de variables.
        int tabla,
            x,
            resultado;
        
        //entrada de datos.
        tabla = Integer.parseInt(JOptionPane.showInputDialog("Ingresa la tabla "
                + "de multiplicar que deseas"));
        
        System.out.println("Tabla de multiplicar del " + tabla + "\n");
        
        for(x = 0; x <= 10; x++){
            //realiza la multitplicacion del numero que se ingreso por la posicion de x.
            resultado = tabla * x;
            //Impresion de resultados.
            System.out.print( tabla + " x " + x + " = " + resultado + "\n");
            
        }//Fin ciclo for.
    }//Fin metodo principal.
}//Fin de la clase E3_Tablas_RaulHerandezLopez.
