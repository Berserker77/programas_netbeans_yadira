/*
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenrgy1975@gmail.com
 * 24 de Noviembre del 2020
 * Grupo: 3012
 * EN UNA TIENDA DEPARTAMENTAL ESTA PUBLICANDO NUEVAS OFERTAS POR
 * TEMPORADA NAVIDEÑA. SI LOS CLIENTES PRESENTAN UNA TARJETA DE CLIENTE
 * FRECUENTE. LES OTORGARAN UN DESCUENTO DEL 20% DEL TOTAL DE SU COMPRA.
 */


package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;

public class E2_CondSimp_RaulHernandezLopez {
    public static void main (String args[]){
        //Declaracion de variables
        String producto,
               seleccion,
               entrada,
               codigo_autentificacion = "R20S";
        double precio,
               monto_a_pagar,
               descuento = 0.80;//Este valor se multiplicara con el precio
                                //para obtener el descuento del 20%.
               
        //Obtiene nombre del producto que se pretende comprar.
        producto = JOptionPane.showInputDialog
        ("nombre del producto");
        //Obtiene el precio del producto.
        precio = Float.parseFloat(JOptionPane.showInputDialog
        ("Ingresa el precio del producto"));
        //Determina si el usuario cuenta con tarjeta o no.
        seleccion = JOptionPane.showInputDialog
        ("Cuentas con tarjeta de cliente frecuente ? yes/no");
        
        
        if(seleccion.equalsIgnoreCase("yes")){
            
            monto_a_pagar = precio * descuento;
            
            JOptionPane.showMessageDialog
            (null, producto + " Monto a pagar $ " + monto_a_pagar);
            
            /*
            //Solicita al usuario que ingrese un cogido de confirmacion.
            entrada = JOptionPane.showInputDialog
            ("ingresa el codigo de auntentificacion");
            //Confirma que el codigo sea correcto
            if(entrada.equals(codigo_autentificacion)){
                //Determina el monto a pagar.
                monto_a_pagar = precio * descuento;
               
                JOptionPane.showMessageDialog
                (null, "Confirmacion exitosa!");
                //Muestra el monto a pagar con un 20% de descuento. 
                JOptionPane.showMessageDialog
                (null, producto + " Monto a pagar $ " + monto_a_pagar);
            }
            
            else {
                //Muestra un mensaje si el codigo es incorrecto.
                JOptionPane.showMessageDialog
                (null, "Confirmacion fallida, por favor vuelve a comenzar");
            }*/
        }
        /*//Muestar el monto a pagar 
        else{
            JOptionPane.showMessageDialog
            (null, producto + "Monto a pagar $" + precio);
        }*/
    }
}
