/*
 * HERNANDEZ_LOPEZ_RAUL @Neo
 * freeenergy1975@gmail.com
 * 8 de Diciembre del 2020
 * Tema_Estructuras ciclicas
 * 
    1. EN EL TECNOLOGICO DE JILOTEPEC SE REALIZARÁ UNA ENCUESTA, PARA CONOCER 
       MAYOR INFORMACION DE LOS ALUMNOS Y VERIFICAR QUIENES DE ELLOS TIENEN O 
       HAN TENIDO ALGUNA DIFICULTAD DE CONECTIVIDAD.  PARA LO CUAL LES ESTAN 
       HACIENDO UN CUESTIONARIO CON LAS SIGUIENTES PREGUNTAS:
        • EDAD
        • LOCALIDAD
        • TIENEN INTERNET EN CASA
        • TIENEN COMPUTADORA EN CASA
        • CUENTA CON LOS RECURSOS BASICOS NECESARIOS DE ALIMENTACION.
    2. RECOLECTA LOS DATOS Y PRESENTA LOS RESULTADOS CONSIDERANDO QUE SE 
       DESCONOCE LA CANTIDAD DE ALUMNOS. 
    3. IMPRIME LOS RESULTADOS CON NUMEROS Y PORCENTAJES. 
 */
package com.mycompany.trabajo_en_clase;

import javax.swing.JOptionPane;

public class ValleProgramacion_Raul_Hernandez_Lopez {

    public static void main(String Neo[]) {
        //Declaracion de variables
        int x,
                numeroAlumnos,
                conComputadora = 0,
                sinComputadora = 0,
                alimentacionAdecuada = 0,
                alimentacionMala = 0,
                conInternet = 0,
                sinInternet = 0,
                edadrango17_19 = 0,
                edadrango20_22 = 0,
                edadrango23_25 = 0,
                edadrango26_28 = 0,
                jilotepecDeMolina = 0,
                aguaEscondida = 0,
                lasHuertas = 0,
                lasManzanas = 0,
                llanoGrande = 0,
                danxho = 0,
                ejidoJilotepec = 0,
                elFresno = 0,
                sanVicente = 0,
                otro = 0;

        String internet,
                computadora,
                alimentacion,
                localidadExterna,
                decicsion;
        float alumnosConComputadora,
                alumnosConInternet,
                alumnosConbuenaAlimentacion,
                alumnosSinComputadora,
                alumnosSinInternet,
                alumnosConMalaAlimentacion,
                rangoedad19,
                rangoedad22,
                rangoedad25,
                rangoedad28,
                porcentaje,
                porcentajeJilotepecDeMol,
                porcentajeAguaEsc,
                porcentajeLasHue,
                porcentajeLasMan,
                porcentajeLLanoGran,
                porcentajeDanxho,
                porcentajeEjidoJil,
                porcentajeElFres,
                porcentajeSanVin,
                porcentajeOtro;
        do {
            //Determina las veces que se aplicara la encuesta
            numeroAlumnos = Integer.parseInt(JOptionPane.showInputDialog("Numero de"
                    + " alumnos a los que se aplicara la encuesta"));
            //Declacion de arrays que almacenaran los datos de los estudiantes
            String nombres[] = new String[numeroAlumnos];
            String localidad[] = new String[numeroAlumnos];
            int edad[] = new int[numeroAlumnos];

            //Aplica la encuentas de acuerdo al numero de alumnos
            for (x = 0; x < numeroAlumnos; x++) {
                //Recopilacion de datos
                nombres[x] = JOptionPane.showInputDialog("Nombre del alumno");

                edad[x] = Integer.parseInt(JOptionPane.showInputDialog("Edad"));

                localidad[x] = JOptionPane.showInputDialog("Digita el número de tu"
                        + " localidad \n1.-jilotepec de Molina Enrriquez \n2.-Agua escondida"
                        + "\n3.-Las Huertas \n4.-Las Manzanas"
                        + "\n5.-Llano Grande \n6.-Danxhó"
                        + "\n7.-Ejido de jilotepec \n8.-El Fresno"
                        + "\n9.-San Vicente \n10.-Otro");

                internet = JOptionPane.showInputDialog("Cuenta con internet en casa "
                        + "si/no");

                computadora = JOptionPane.showInputDialog("Cuenta con computadora"
                        + "si/no");

                alimentacion = JOptionPane.showInputDialog("Cuenta los recursos "
                        + "basicos para la alimentacion si/no");
                //Evalua el rango de edad.
                if ((edad[x] >= 17) && (edad[x] <= 19)) {
                    edadrango17_19 += 1;
                }//Fin if 1
                else {
                    if ((edad[x] >= 20) && (edad[x] <= 22)) {
                        edadrango20_22 += 1;
                    }//Fin if 1.1
                    else {
                        if ((edad[x] >= 23) && (edad[x] <= 25)) {
                            edadrango23_25 += 1;
                        }//Fin if 1.2
                        else {
                            if ((edad[x] >= 26) && (edad[x] <= 28)) {
                                edadrango26_28 += 1;
                            }//Fin if 1.3
                        }//Fin else 1.2
                    }//Fin else 1.1
                }//Fin else 1

                //Evalua la localidad seleccionada
                if (localidad[x].equals("1")) {
                    jilotepecDeMolina += 1;
                }//Fin if 1a
                else {
                    if (localidad[x].equals("2")) {
                        aguaEscondida += 1;
                    }//Fin if 1b
                    else {
                        if (localidad[x].equals("3")) {
                            lasHuertas += 1;
                        }//Fin if 1c
                        else {
                            if (localidad[x].equals("4")) {
                                lasManzanas += 1;
                            }//Fin if 1d
                            else {
                                if (localidad[x].equals("5")) {
                                    llanoGrande += 1;
                                }//Fin if 1e
                                else {
                                    if (localidad[x].equals("6")) {
                                        danxho += 1;
                                    }//Fin if 1d
                                    else {
                                        if (localidad[x].equals("7")) {
                                            ejidoJilotepec += 1;
                                        }//Fin if 1g
                                        else {
                                            if (localidad[x].equals("8")) {
                                                elFresno += 1;
                                            }//Fin if 1h
                                            else {
                                                if (localidad[x].equals("9")) {
                                                    sanVicente = +1;
                                                }//Fin if 1i
                                                else {
                                                    if (localidad[x].equals("10")) {
                                                        otro += 1;
                                                        localidadExterna = JOptionPane
                                                                .showInputDialog("Por favor especifica");
                                                    }
                                                }//Fin else 1i
                                            }//Fin else 1h
                                        }//Fin else 1g
                                    }//Fin else 1f
                                }//Fin else 1e
                            }//Fin else 1d
                        }//Fin else 1c
                    }//Fin else1b
                }//Fin else 1a
                //Evalua si el alumno cuenta con los recursos necesarios
                if (internet.equalsIgnoreCase("si")) {
                    conInternet += 1;
                }//Fin if 2
                else {
                    sinInternet += 1;
                }//Fin else 2

                if (computadora.equalsIgnoreCase("si")) {
                    conComputadora += 1;
                }//Fin if 3
                else {
                    sinComputadora += 1;
                }//Fin else 3

                if (alimentacion.equalsIgnoreCase("si")) {
                    alimentacionAdecuada += 1;
                }//Fin if 4
                else {
                    alimentacionMala += 1;
                }//Fin else 4
            }//Fin for
            //define el numero de alumnos como el 100%
            porcentaje = (100 / numeroAlumnos);
            //Determina el porcentaje que le corresponde a cada aspecto
            alumnosConComputadora = porcentaje * conComputadora;
            alumnosSinComputadora = porcentaje * sinComputadora;
            alumnosConInternet = porcentaje * conInternet;
            alumnosSinInternet = porcentaje * sinInternet;
            alumnosConbuenaAlimentacion = porcentaje * alimentacionAdecuada;
            alumnosConMalaAlimentacion = porcentaje * alimentacionMala;
            //Calcula el porcentaje correspondiente al rango de edades
            rangoedad19 = porcentaje * edadrango17_19;
            rangoedad22 = porcentaje * edadrango20_22;
            rangoedad25 = porcentaje * edadrango23_25;
            rangoedad28 = porcentaje * edadrango26_28;
            //Calcula el porcentaje correspondiente a las localidades
            porcentajeJilotepecDeMol = porcentaje * jilotepecDeMolina;
            porcentajeAguaEsc = porcentaje * aguaEscondida;
            porcentajeLasHue = porcentaje * lasHuertas;
            porcentajeLasMan = porcentaje * lasManzanas;
            porcentajeLLanoGran = porcentaje * llanoGrande;
            porcentajeDanxho = porcentaje * danxho;
            porcentajeEjidoJil = porcentaje * ejidoJilotepec;
            porcentajeElFres = porcentaje * elFresno;
            porcentajeSanVin = porcentaje * sanVicente;
            porcentajeOtro = porcentaje * otro;

            //Imprecion de resultados
            JOptionPane.showMessageDialog(null, "Se aplico la encuesta a "
                    + numeroAlumnos + " alumnos del TESJI\n"
                    + "los resultados son los siguientes");

            JOptionPane.showMessageDialog(null, "De la comunidad estudiantil del TESJI el "
                    + alumnosConComputadora + "% de los alumnos cuenta con"
                    + " computadora que corresonden a " + conComputadora
                    + " alumnos"
                    + "\nMientras que el " + alumnosSinComputadora + "% no,"
                    + " los cuales son " + sinComputadora + " alumnos.");

            JOptionPane.showMessageDialog(null, "El " + alumnosConInternet
                    + "% de los alumos tiene internet que corresponden a "
                    + conInternet + " alumnos."
                    + "\n mientras que el " + alumnosSinInternet + "% no, "
                    + "y corresponden a " + sinInternet + " alumnos.");

            JOptionPane.showMessageDialog(null, "El " + alumnosConbuenaAlimentacion
                    + "% cuenta con los medios necesarios para una buena alimentacion"
                    + " esta cantidad corresponde a " + alimentacionAdecuada
                    + " alumnos."
                    + "\nmientras que el " + alumnosConMalaAlimentacion
                    + "% no, lo que corresponde a la cantidad de " + alimentacionMala
                    + " alumnos.");
            JOptionPane.showMessageDialog(null, "Las edades son las siguientes"
                    + "\nEl " + rangoedad19 + "% tiene entre 17 y 20 años "
                    + " que corresponden a " + edadrango17_19 + " alumnos"
                    + "\nEl " + rangoedad22 + "% tiene entre 21 y 22 años"
                    + " que corresponden a " + edadrango20_22 + " alumnos"
                    + "\nEl " + rangoedad25 + "% tiene entre 23 y 25 años"
                    + " que corresponden a " + edadrango23_25 + " alumnos"
                    + "\nEl " + rangoedad28 + "% tiene entre 26 y 28 años"
                    + " que corresponden a " + edadrango26_28 + " alumnos");

            JOptionPane.showMessageDialog(null, "Las localidades de procedencia son las siguientes\n"
                    + "\nEl " + porcentajeJilotepecDeMol + "% proviene de Jitotepec de Molina Enrriquez"
                    + " el numero de alumnos perteneciente a esta localidad es "
                    + jilotepecDeMolina
                    + "\nEl " + porcentajeAguaEsc + "% proviene de Agua Escondida"
                    + " el numero de alumnos perteneciente a esta localidad es "
                    + aguaEscondida
                    + "\nEl " + porcentajeLasHue + "% proviene de Las Huertas"
                    + " el numero de alumnos pertenecientes a esta localidad es "
                    + lasHuertas
                    + "\nEl " + porcentajeLasMan + "% proviene de Las Manzanas"
                    + " el numero de alumnos pertenecientes a esta localidad es "
                    + lasManzanas
                    + "\nEl " + porcentajeLLanoGran + "% proviene de LLano grande"
                    + " el numero de alumnos pertenecientes a esta localidad es "
                    + llanoGrande
                    + "\nEl " + porcentajeDanxho + "% proviene de Danxhó"
                    + " el numero de alumnos pertenecientes a esta localidad es "
                    + danxho
                    + "\nEl " + porcentajeEjidoJil + "% proviene del Ejido de Jilotepec"
                    + " el numero de alumnos pertenecientes a esta localidad es "
                    + ejidoJilotepec
                    + "\nEl " + porcentajeElFres + "%proviene del Fresno"
                    + " el numero de alumnos pertenecientes a esta localidad es "
                    + elFresno
                    + "\nEl " + porcentajeSanVin + "% proviene de San Vicente"
                    + " el numero de alumnos pertenecientes a esta localidad es "
                    + sanVicente
                    + "\nEl " + porcentajeOtro + "% proviene de otros lugares"
                    + " el numero de alumnos pertenecientes a esta localidad es "
                    + otro + "\n\n");

            decicsion = JOptionPane.showInputDialog("Deseas salir de la aplicacion si/no");

        } while (decicsion.equalsIgnoreCase("no"));
    }//Fin metodo principal
}//Fin ValleProgramacion_Raul_Hernandez_Lopez.